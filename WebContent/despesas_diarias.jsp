
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="Java" import="br.com.gerenciador.dao.CategoriaDAO"%>
<%@ page language="Java" import="br.com.gerenciador.model.Categorias"%>

<%@ page language="Java"
	import="br.com.gerenciador.conexao.ConnectionFactory"%>

<%@ page language="Java" import="java.sql.Connection"%>
<%@ page language="Java" import="java.util.ArrayList"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Creative - Bootstrap 3 Responsive Admin Template">
<meta name="author" content="GeeksLabs">
<meta name="keyword"
	content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
<link rel="shortcut icon" href="img/favicon.png">

<title>Despesas Di�rias</title>

<!-- Bootstrap CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- bootstrap theme -->
<link href="css/bootstrap-theme.css" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="css/elegant-icons-style.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<!-- date picker -->

<!-- color picker -->
<!-- Custom styles -->
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
<script type="text/javascript">
	
<%Connection conexao = (Connection) new ConnectionFactory().getConnection();
			Long codTemp = null;
			if (conexao != null) {
				CategoriaDAO dao = new CategoriaDAO(conexao);
				ArrayList<Categorias> categorias = dao.select();
				codTemp = dao.selectTemp();
				System.out.println("Atualiza��o Agenda.jsp");
				session.setAttribute("categorias", categorias);
				session.setAttribute("codTemp", codTemp);
			} else {
				System.out.println("Conexao Null Agenda.jsp atualizando agenda sal�o");
			}%>
	
</script>
</head>
<body onmousemove="temporario()">

	<!-- container section start -->
	<section id="container" class="">
		<!--header start-->

		<!--header end-->

		<!--sidebar start-->
		<c:import url="menu.jsp"></c:import>
		<!--sidebar end-->

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">
							<i class="fa fa-file-text-o"></i> Formul�rio
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="index.jsp">P�gina
									Principal</a></li>
							<li><i class="fa fa-file-text-o"></i>Despesas</li>
						</ol>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
							<header class="panel-heading"> Preencha o formul�rio </header>
							<div class="panel-body">
								<form class="form-horizontal" method="post" action="Financas">
									<input name="acao" value="DespesasDiarias" type="hidden">
									<input type="hidden" name="tipo" value="despesas">
									<div class="form-group">
										<label class="col-sm-2 control-label">Descri��o da
											Despesa</label>
										<div class="col-sm-10">
											<input type="text" name="nome"
												class="form-control round-input" required="required">
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">Valor da Despesa</label>
										<div class="col-sm-10">
											<input type="text" name="valor" id="valor"
												class="form-control round-input" required="required">
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">Total de parcelas</label>
										<div class="col-sm-10">
											<input type="number" name="parcelas" id="parcelas"
												class="form-control round-input" required="required">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Classe de
											Categoria </label>
										<div class="col-sm-10">
											<select style="width: 100%;" class="form-control select2"
												data-placeholder="Select a State" name="categoria"
												id="categoria">
												<c:forEach var="categoria" items="${categorias}"
													varStatus="id">
													<option value="${categoria.cod}">${categoria.categoria}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<!-- 
                                    <div class="form-group">
										<label class="col-sm-2 control-label">Data de In�cio</label>
										<div class="col-sm-10">
											<input type="text" pattern="\d{2}/\d{2}/\d{4}" name="dataIni" id="dataIni"
												class="form-control round-input" required="required">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-2 control-label">Data Termino</label>
										<div class="col-sm-10">
											<input type="text" pattern="\d{1,2}/\d{1,2}/\d{4}" name="dataTer" id="dataTer"
												class="form-control round-input" required="required">
										</div>
									</div>
 -->
									<button class="btn btn-info" type="submit">Cadastrar</button>
								</form>

							</div>
						</section>
					</div>
				</div>
			</section>
		</section>
		<!--main content end-->
	</section>
	<!-- container section end -->
	<!-- javascripts -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- nice scroll -->
	<script src="js/jquery.scrollTo.min.js"></script>
	<script src="js/jquery.nicescroll.js" type="text/javascript"></script>

	<!-- jquery ui -->
	<script src="js/jquery-ui-1.9.2.custom.min.js"></script>

	<!--custom checkbox & radio-->
	<script type="text/javascript" src="js/ga.js"></script>
	<!--custom switch-->
	<script src="js/bootstrap-switch.js"></script>
	<!--custom tagsinput-->
	<script src="js/jquery.tagsinput.js"></script>

	<!-- colorpicker -->

	<!-- bootstrap-wysiwyg -->
	<script src="js/jquery.hotkeys.js"></script>
	<script src="js/bootstrap-wysiwyg.js"></script>
	<script src="js/bootstrap-wysiwyg-custom.js"></script>
	<!-- ck editor -->
	<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
	<!-- custom form component script for this page-->
	<script src="js/form-component.js"></script>
	<!-- custome script for all page -->
	<script src="js/scripts.js"></script>
	<script src="js/jquery.maskMoney.js"></script>
	<script src="js/jquery.maskMoney.min.js"></script>

	<script src="js/jquery.inputmask.js"></script>
	<script src="js/jquery.inputmask.date.extensions.js"></script>
	<script src="js/jquery.inputmask.extensions.js"></script>


	<script type="text/javascript">
		$("#valor").maskMoney();
		$("#dataIni").inputmask("dd/mm/yyyy", {
			"placeholder" : "dd/mm/yyyy"
		});
		$("#dataTer").inputmask("dd/mm/yyyy", {
			"placeholder" : "dd/mm/yyyy"
		});
		function temporario() {

			if (document.getElementById("categoria").value ==
	<%=codTemp%>
		) {
				document.getElementById("dataIni").disabled = false;
				document.getElementById("dataTer").disabled = false;
			} else {
				document.getElementById("dataIni").disabled = true;
				document.getElementById("dataTer").disabled = true;
			}
		}
	</script>

</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="Java" import="br.com.gerenciador.dao.BancoDAO"%>

<%@ page language="Java"
	import="br.com.gerenciador.conexao.ConnectionFactory"%>
<%@ page language="Java" import="br.com.gerenciador.model.Usuario"%>
<%@ page language="Java" import="java.sql.Connection"%>
<%@ page language="Java" import="java.util.ArrayList"%>
<!DOCTYPE html>

<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Creative - Bootstrap 3 Responsive Admin Template">
<meta name="author" content="GeeksLabs">
<meta name="keyword"
	content="Creative, P�gina Principal, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
<link rel="shortcut icon" href="img/favicon.png">

<title>Sistema Financeiro</title>

<script type="text/javascript">
	
<%Connection conexao = (Connection) new ConnectionFactory().getConnection();
			Usuario usuario = (Usuario) session.getAttribute("usuariologado");
			if (conexao != null) {
				BancoDAO dao = new BancoDAO(conexao);
				String valor = dao.selectValor(usuario.getCod());
				String totalDespesas = dao.selectValorDespesaMensal(usuario.getCod());
				String valorReceita = dao.selectValorEntradaMensal(usuario.getCod());
				String valorPlanejado = dao.selectValorPlanejadoMensal(usuario.getCod());
				float IDC = new Float(valorPlanejado) / new Float(totalDespesas);
				String situacao = null;
				if (IDC > 1) {
					situacao = "No Planejado";
				} else {
					situacao = "Plano Estourado";
				}
				System.out.println("Atualiza��o index.jsp");
				session.setAttribute("valor", valor);
				session.setAttribute("totalDespesas", totalDespesas);
				session.setAttribute("valorReceita", valorReceita);

				session.setAttribute("valorPlanejado", valorPlanejado);
				session.setAttribute("IDC", situacao);
			} else {
				System.out.println("Conexao Null tabela_despesas.jsp atualizando ");
			}%>
	
</script>
<!-- Bootstrap CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- bootstrap theme -->
<link href="css/bootstrap-theme.css" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="css/elegant-icons-style.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<!-- full calendar css-->
<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"
	rel="stylesheet" />
<link href="assets/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" />
<!-- easy pie chart-->
<link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<!-- owl carousel -->
<link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
<link href="css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<!-- Custom styles -->
<link rel="stylesheet" href="css/fullcalendar.css">
<link href="css/widgets.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />
<link href="css/xcharts.min.css" rel=" stylesheet">
<link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
</head>
<body>
	<!-- container section start -->
	<section id="container" class="">

		<c:import url="menu.jsp"></c:import>
		<!--sidebar end-->

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<!--overview start-->
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> P�gina Principal
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-laptop"></i><a href="index.jsp">P�gina Principal</a></li>
							
						</ol>
					</div>
				</div>

				<c:choose>
					<c:when test="${not empty msgUsuario}">
						<div class="box-body">
							<div class="alert alert-danger alert-dismissible">
								<button type="button" class="close" data-dismiss="alert"
									aria-hidden="true">&times;</button>
								<h4>
									<i class="icon fa fa-ban"></i> Alerta!
								</h4>
								${msgUsuario}
							</div>
						</div>
					</c:when>
				</c:choose>

				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="info-box blue-bg">
							<div class="count">${IDC}</div>
							<div class="title">Calculo GVA</div>
						</div>
						<!--/.info-box-->
					</div>
					<!--/.col-->

					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="info-box brown-bg">
							<i class="fa fa-shopping-cart"></i>
							<div class="count">${totalDespesas}</div>
							<div class="title">TOTAL DE DESPESA NO M�S</div>
						</div>
						<!--/.info-box-->
					</div>
					<!--/.col-->

					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="info-box green-bg">
							<i class="fa fa-thumbs-o-up"></i>
							<div class="count">${valor}</div>
							<div class="title">Valor atual do Banco</div>
						</div>
						<!--/.info-box-->
					</div>
					<!--/.col-->

					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="info-box green-bg">
							<i class="fa fa-cubes"></i>
							<div class="count">${valorPlanejado}</div>
							<div class="title">Valor Planejado</div>
						</div>
						<!--/.info-box-->
					</div>
					<!--/.col-->

				</div>

				<div class="col-lg-12">
					<section class="panel">
						<header class="panel-heading">
							<h3>
								General Chart
								</Char>
						</header>
						<div class="panel-body">
							<div class="tab-pane" id="chartjs">
								<div class="row">
									<!-- Line -->
									<div class="col-lg-6">
										<section class="panel">
											<header class="panel-heading"> Line </header>
											<div class="panel-body text-center">
												<canvas id="line" height="300" width="450"></canvas>
											</div>
										</section>
									</div>
									<!-- Bar -->
									<div class="col-lg-6">
										<section class="panel">
											<header class="panel-heading"> Bar </header>
											<div class="panel-body text-center">
												<canvas id="bar" height="300" width="500"></canvas>
											</div>
										</section>
									</div>
								</div>
								<div class="row">
									<!-- Radar -->
									<div class="col-lg-6">
										<section class="panel">
											<header class="panel-heading"> Radar </header>
											<div class="panel-body text-center">
												<canvas id="radar" height="300" width="400"></canvas>
											</div>
										</section>
									</div>
									<!-- Polar Area -->
									<div class="col-lg-6">
										<section class="panel">
											<header class="panel-heading"> Polar Area </header>
											<div class="panel-body text-center">
												<canvas id="polarArea" height="300" width="400"></canvas>
											</div>
										</section>
									</div>
								</div>
								<div class="row">

									<!-- Pie -->
									<div class="col-lg-6">
										<section class="panel">
											<header class="panel-heading"> Pie </header>
											<div class="panel-body text-center">
												<canvas id="pie" height="300" width="400"></canvas>
											</div>
										</section>
									</div>
									<!-- Doughnut -->
									<div class="col-lg-6">
										<section class="panel">
											<header class="panel-heading"> Doughnut </header>
											<div class="panel-body text-center">
												<canvas id="doughnut" height="300" width="400"></canvas>
											</div>
										</section>
									</div>
								</div>
							</div>
						</div>
				</div>
				<!-- container section start -->

				<!-- javascripts -->
				<script src="js/jquery-ui-1.10.4.min.js"></script>
				<script src="js/jquery-1.8.3.min.js"></script>
				<script type="text/javascript"
					src="js/jquery-ui-1.9.2.custom.min.js"></script>
				<!-- bootstrap -->
				<script src="js/bootstrap.min.js"></script>
				<!-- nice scroll -->
				<script src="js/jquery.scrollTo.min.js"></script>
				<script src="js/jquery.nicescroll.js" type="text/javascript"></script>
				<!-- charts scripts -->
				<script src="assets/jquery-knob/js/jquery.knob.js"></script>
				<script src="js/jquery.sparkline.js" type="text/javascript"></script>
				<script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
				<script src="js/owl.carousel.js"></script>
				<!-- jQuery full calendar -->
				<
				<script src="js/fullcalendar.min.js"></script>
				<!-- Full Google Calendar - Calendar -->
				<script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
				<!--script for this page only-->
				<script src="js/calendar-custom.js"></script>
				<script src="js/jquery.rateit.min.js"></script>
				<!-- custom select -->
				<script src="js/jquery.customSelect.min.js"></script>
				<script src="assets/chart-master/Chart.js"></script>

				<!--custome script for all page-->
				<script src="js/scripts.js"></script>
				<!-- custom script for this page-->
				<script src="js/sparkline-chart.js"></script>
				<script src="js/easy-pie-chart.js"></script>
				<script src="js/jquery-jvectormap-1.2.2.min.js"></script>
				<script src="js/jquery-jvectormap-world-mill-en.js"></script>
				<script src="js/xcharts.min.js"></script>
				<script src="js/jquery.autosize.min.js"></script>
				<script src="js/jquery.placeholder.min.js"></script>
				<script src="js/gdp-data.js"></script>
				<script src="js/morris.min.js"></script>
				<script src="js/sparklines.js"></script>
				<script src="js/charts.js"></script>
				<script src="js/jquery.slimscroll.min.js"></script>
				<script src="js/jquery.js"></script>
				<script src="js/jquery-1.8.3.min.js"></script>
				<script src="js/bootstrap.min.js"></script>
				<!-- nice scroll -->
				<script src="js/jquery.scrollTo.min.js"></script>
				<script src="js/jquery.nicescroll.js" type="text/javascript"></script>
				<!-- chartjs -->
				<script src="assets/chart-master/Chart.js"></script>
				<!-- custom chart script for this page only-->
				<script src="js/chartjs-custom.js"></script>
				<!--custome script for all page-->
				<script type="text/javascript">
					var pieChartCanvas = $("#pie").get(0).getContext("2d");
					var pieChart = new Chart(pieChartCanvas);
					var PieData = [ {
						value : 700,
						color : "#f56954",
						highlight : "#f56954",
						label : "Chrome"
					}, {
						value : 500,
						color : "#00a65a",
						highlight : "#00a65a",
						label : "IE"
					}, {
						value : 400,
						color : "#f39c12",
						highlight : "#f39c12",
						label : "FireFox"
					}, {
						value : 600,
						color : "#00c0ef",
						highlight : "#00c0ef",
						label : "Safari"
					}, {
						value : 300,
						color : "#3c8dbc",
						highlight : "#3c8dbc",
						label : "Opera"
					}, {
						value : 100,
						color : "#d2d6de",
						highlight : "#d2d6de",
						label : "Navigator"
					} ];

					pieChart.Doughnut(PieData, pieOptions);
				</script>
</body>
</html>

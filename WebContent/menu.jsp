<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Creative - Bootstrap 3 Responsive Admin Template">
<meta name="author" content="GeeksLabs">
<meta name="keyword"
	content="Creative, P�gina Principal, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
<link rel="shortcut icon" href="img/favicon.png">



<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
</head>

<body>

	<header class="header dark-bg">
		<div class="toggle-nav">
			<div class="icon-reorder tooltips"
				data-original-title="Ativar Menu Principal" data-placement="bottom">
				<i class="icon_menu"></i>
			</div>
		</div>
		<a href="http://45.55.154.243/MinhasFinancas/index.jsp" class="logo">Minhas
			<span class="lite">Finan�as</span>
		</a>
		<div class="top-nav notification-row">
			<a class="logo">Usu�rio : <span class="lite">${ usuariologado.nome}</span></a>
		</div>
	</header>

	<!-- container section start -->
	<aside>
		<div id="sidebar" class="nav-collapse ">
			<!-- sidebar menu start-->
			<ul class="sidebar-menu">
				<li class="active"><a class=""
					href="http://45.55.154.243/MinhasFinancas/index.jsp"> <i
						class="icon_house_alt"></i> <span>P�gina Principal</span>
				</a></li>

				<li><a class="" href="despesas_diarias.jsp"> <i
						class="fa fa-book fa-fw"></i> <span>Despesas</span>
				</a></li>
				<li><a class="" href="dinheiro_extra.jsp"> <i
						class="fa fa-book fa-fw"></i> <span>Receitas</span>
				</a></li>


				<li class="sub-menu"><a href="javascript:;" class=""> <i
						class="icon_table"></i> <span>Movimenta��o </span> <span
						class="menu-arrow arrow_carrot-right"></span>
				</a>
					<ul class="sub">
						<li><a class=""
							href="http://45.55.154.243/MinhasFinancas/tabela_despesa.jsp">De
								Despesas No M�s</a></li>
						<li><a class=""
							href="http://45.55.154.243/MinhasFinancas/tabela_entrada.jsp">De
								Entradas No M�s</a></li>
					</ul></li>

				<li><a class=""
					href="http://45.55.154.243/MinhasFinancas/planejamento.jsp"> <i
						class="fa fa-book fa-fw"></i> <span>Planejamento</span>
				</a></li>


				<li class="sub-menu"><a href="javascript:;" class=""> <i
						class="fa fa-cog fa-fw"></i> <span>Configura��es</span> <span
						class="menu-arrow arrow_carrot-right"></span>
				</a>
					<ul class="sub">
						<!--  <li><a class="" href="salario_mes.jsp">Sal�rio do M�s</a></li>    -->
						<li><a class=""
							href="http://45.55.154.243/MinhasFinancas/configurar_senha.jsp">Alterar
								Senha</a></li>
					</ul></li>


				<li><a class=""
					href="http://45.55.154.243/MinhasFinancas/Financas?acao=Sair">
						<i class="fa fa-sign-out"></i> <span>Sair</span>
				</a></li>
				<!-- 
				<li class="sub-menu"><a href="javascript:;" class=""> <i
						class="icon_document_alt"></i> <span>Forms</span> <span
						class="menu-arrow arrow_carrot-right"></span>
				</a>
					<ul class="sub">
						<li><a class="" href="form_component.html">Form Elements</a></li>
						<li><a class="" href="form_validation.html">Form
								Validation</a></li>
					</ul></li>
				<li class="sub-menu"><a href="javascript:;" class=""> <i
						class="icon_desktop"></i> <span>UI Fitures</span> <span
						class="menu-arrow arrow_carrot-right"></span>
				</a>
					<ul class="sub">
						<li><a class="" href="general.html">Elements</a></li>
						<li><a class="" href="buttons.html">Buttons</a></li>
						<li><a class="" href="grids.html">Grids</a></li>
					</ul></li>
				<li><a class="" href="widgets.html"> <i class="icon_genius"></i>
						<span>Widgets</span>
				</a></li>
				<li><a class="" href="chart-chartjs.html"> <i
						class="icon_piechart"></i> <span>Charts</span>

				</a></li>

				<li class="sub-menu"><a href="javascript:;" class=""> <i
						class="icon_table"></i> <span>Tables</span> <span
						class="menu-arrow arrow_carrot-right"></span>
				</a>
					<ul class="sub">
						<li><a class="" href="basic_table.html">Basic Table</a></li>
					</ul></li>

				<li class="sub-menu"><a href="javascript:;" class=""> <i
						class="icon_documents_alt"></i> <span>Pages</span> <span
						class="menu-arrow arrow_carrot-right"></span>
				</a>
					<ul class="sub">
						<li><a class="" href="profile.html">Profile</a></li>
						<li><a class="" href="login.jsp"><span>Login Page</span></a></li>
						<li><a class="" href="blank.html">Blank Page</a></li>
						<li><a class="" href="404.html">404 Error</a></li>
					</ul></li>

			</ul>
			<!-- sidebar menu end-->
		</div>
	</aside>



</body>
</html>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Creative - Bootstrap 3 Responsive Admin Template">
<meta name="author" content="GeeksLabs">
<meta name="keyword"
	content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
<link rel="shortcut icon" href="img/favicon.png">

<title>Sal�rio Mensal</title>

<!-- Bootstrap CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- bootstrap theme -->
<link href="css/bootstrap-theme.css" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="css/elegant-icons-style.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<!-- date picker -->

<!-- color picker -->
<!-- Custom styles -->
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

</head>
<body>

	<!-- container section start -->
	<section id="container" class="">
		<!--header start-->
		
		<!--header end-->

		<!--sidebar start-->
		<c:import url="menu.jsp"></c:import>
		<!--sidebar end-->

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">
							<i class="fa fa-file-text-o"></i> Form elements
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="index.jsp">P�gina Principal</a></li>
							<li><i class="icon_document_alt"></i>Entrada</li>
							<li><i class="fa fa-file-text-o"></i>Sal�rio Mensal</li>
						</ol>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
							<header class="panel-heading"> Preencha o formul�rio </header>
							<div class="panel-body">
								<form class="form-horizontal" method="post" action="Financas">
								<input name="acao" value="SalarioMensal" type="hidden">
							    <input type="hidden" name="tipo" value="entrada">
									<div class="form-group">
										<label class="col-sm-2 control-label">Descri��o do
											Sal�rio</label>
										<div class="col-sm-10">
											<input type="text" name="nome"
												class="form-control round-input" required="required">
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">Valor da Despesa</label>
										<div class="col-sm-10">
											<input type="text" name="valor" id="valor"
												class="form-control round-input" required="required">
										</div>
									</div>

                                 
                                        <button class="btn btn-info" type="submit">Cadastrar</button>
								</form>

							</div>
						</section>
					</div>
				</div>
			</section>
		</section>
		<!--main content end-->
	</section>
	<!-- container section end -->
	<!-- javascripts -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- nice scroll -->
	<script src="js/jquery.scrollTo.min.js"></script>
	<script src="js/jquery.nicescroll.js" type="text/javascript"></script>

	<!-- jquery ui -->
	<script src="js/jquery-ui-1.9.2.custom.min.js"></script>

	<!--custom checkbox & radio-->
	<script type="text/javascript" src="js/ga.js"></script>
	<!--custom switch-->
	<script src="js/bootstrap-switch.js"></script>
	<!--custom tagsinput-->
	<script src="js/jquery.tagsinput.js"></script>

	<!-- colorpicker -->

	<!-- bootstrap-wysiwyg -->
	<script src="js/jquery.hotkeys.js"></script>
	<script src="js/bootstrap-wysiwyg.js"></script>
	<script src="js/bootstrap-wysiwyg-custom.js"></script>
	<!-- ck editor -->
	<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
	<!-- custom form component script for this page-->
	<script src="js/form-component.js"></script>
	<!-- custome script for all page -->
	<script src="js/scripts.js"></script>
	<script src="js/jquery.maskMoney.js"></script>
<script src="js/jquery.maskMoney.min.js"></script>
	
<script type="text/javascript">
$("#valor").maskMoney();
</script>

</body>
</html>

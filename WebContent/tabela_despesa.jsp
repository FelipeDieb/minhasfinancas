<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="Java" import="br.com.gerenciador.dao.DespesasDAO"%>

<%@ page language="Java"
	import="br.com.gerenciador.conexao.ConnectionFactory"%>
<%@ page language="Java" import="br.com.gerenciador.model.Despesas"%>
<%@ page language="Java" import="br.com.gerenciador.model.Usuario"%>
<%@ page language="Java" import="java.sql.Connection"%>
<%@ page language="Java" import="java.util.ArrayList"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script type="text/javascript">
	
<%Connection conexao = (Connection) new ConnectionFactory().getConnection();
			Usuario usuario = (Usuario) session.getAttribute("usuariologado");
			if (conexao != null) {
				DespesasDAO dao = new DespesasDAO(conexao);
				ArrayList<Despesas> despesas = dao.select(usuario.getCod());

				System.out.println("Atualiza��o Agenda.jsp");
				session.setAttribute("despesas", despesas);
			} else {
				System.out.println("Conexao Null tabela_despesas.jsp atualizando ");
			}%>
	
</script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Creative - Bootstrap 3 Responsive Admin Template">
<meta name="author" content="GeeksLabs">
<meta name="keyword"
	content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
<link rel="shortcut icon" href="img/favicon.png">

<title>Tabela de despesas</title>

<!-- Bootstrap CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- bootstrap theme -->
<link href="css/bootstrap-theme.css" rel="stylesheet">

<!-- DataTables -->
<link rel="stylesheet"
	href="http://45.55.154.243/MinhasFinancas/css/dataTables.bootstrap.css">
<!--external css-->
<!-- font icon -->
<link href="css/elegant-icons-style.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<!-- Custom styles -->
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
</head>

<body>
	<!-- container section start -->
	<section id="container" class="">
		<!--header start-->

		<!--header end-->

		<!--sidebar start-->
		<c:import url="menu.jsp"></c:import>

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">
							<i class="fa fa-table"></i> Despesas
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="index.jsp">P�gina
									Principal</a></li>
							<li><i class="fa fa-table"></i>Tabelas</li>
							<li><i class="fa fa-th-list"></i>De Despesas</li>
						</ol>
					</div>
				</div>
				<!-- page start-->

				<dl>
					<div class="row">
						<div class="col-sm-2">
							<form action="Financas" method="post" id="forms">
								<input name="acao" value="AjaxDespesas" type="hidden"> <input
									type="hidden" name="tipo" value="despesas"> <input
									type="text" class="form-control pull-right" name="acao1"
									id="dataPesquisa" pattern="\d{2}/\d{4}"
									data-inputmask="'alias': 'mm/yyyy'" data-mask
									placeholder="Data para pesquisa">
							</form>
						</div>
						<div class="col-sm-2">
							<button onclick="pesquisa()" class="btn btn-primary">
								<span class="icon_lightbulb_alt"></span> Pesquisa
							</button>
						</div>
						<!-- <div class="col-sm-1">
							<button data-toggle="modal"
								data-target=".bs-example-modal-relatorio"
								class="btn btn-primary">
								<span class="fa fa-align-justify"></span> Gerar Relat�rio
							</button>
						</div> -->
					</div>
				</dl>
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
							<div class="table-responsive">

								<header class="panel-heading"> Tabela de Despesas </header>
								<table id="tabela" class="table table-bordered table-hover ">
									<thead>
										<tr>
											<th>Descri��o</th>
											<th>Situa��o</th>
											<th>Valor</th>
											<th>Categoria</th>
											<th>Data Incus�o</th>
											<th>Data In�cio</th>
											<th>Data T�rmino</th>
											<th>Realizar Pagamento</th>
											<th>Excluir Pagamento</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="despesa" items="${despesas}" varStatus="id">
											<tr>
												<td>${despesa.nome}</td>
												<td>${despesa.situacao}
												<td>${despesa.valor}</td>
												<td>${despesa.categoria.categoria}</td>
												<td>${despesa.dataInclusao}</td>
												<td>${despesa.dataInicio}</td>
												<td>${despesa.dataTermino}</td>
												<c:choose>
													<c:when
														test="${despesa.situacao == 'Aguardando Pagamento'}">
														<td><button onclick='pagar(${despesa.cod});'
																type="button" class="btn btn-success">
																<span class="icon_check_alt2"></span>
															</button></td>
													</c:when>


													<c:when
														test="${despesa.situacao  != 'Aguardando Pagamento'}">
														<td><button disabled="disabled" onclick=''
																type="button" class="btn btn-success">
																<span class="icon_check_alt2"></span>
															</button></td>
													</c:when>

												</c:choose>
												<td><button onclick='excluir(${despesa.cod})'
														type="button" class="btn btn-danger">
														<span class="fa fa-times"></span>
													</button></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>

						</section>
					</div>
				</div>
				<!-- page end-->
			</section>
			<form action="Financas" method="post" id="formsPaga">
				<input name="acao" value="PagarDespesa" type="hidden"> <input
					type="hidden" name="tipo" value="despesas"> <input
					type="hidden" name="acao1" id="codDespesa">
			</form>
			<form action="Financas" method="post" id="formsCanc">
				<input name="acao" value="ExcluirDespesa" type="hidden"> <input
					type="hidden" name="tipo" value="despesas"> <input
					type="hidden" name="acao1" id="codDespesaCanc">
			</form>
			<!--main content end-->
		</section>

		<div class="modal fade bs-example-modal-relatorio" tabindex="-1"
			role="dialog" aria-labelledby="myLargeModalLabel">
			<div"modal-dialogmodal-lg">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">Gerar Relat�rio</h4>
						</div>
						<form class="form-horizontal" method="post" action="Financas">
							<input type="hidden" name="acao" value="GerarRelatorio">
							<input type="hidden" name="nomeRelatorio" value="despesas">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-2 control-label">Data Inicial</label> <input
										name="acao" value="AjaxDespesas" type="hidden">
									<div class="col-sm-8">
										<input type="text" name="dataInicio" id="dataPesquisaModal"
											class="form-control" pattern="\d{2}/\d{4}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Data Final</label> <input
										name="acao" value="AjaxDespesas" type="hidden">
									<div class="col-sm-8">
										<input type="text" name="dataFim" id="dataPesquisaModal2"
											class="form-control" pattern="\d{2}/\d{4}">
									</div>
								</div>
							</div>
							<!-- /.box-body -->
							<div class="box-footer"></div>

							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left"
									data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Gerar</button>
							</div>
							<!-- /.box-footer -->
						</form>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
		</div>
		<!-- container section end -->
		<!-- javascripts -->
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!-- nicescroll -->
		<script src="js/jquery.scrollTo.min.js"></script>
		<script src="js/jquery.nicescroll.js" type="text/javascript"></script>
		<!--custome script for all page-->
		<script src="js/scripts.js"></script>


		<!-- DataTables -->
		<script src="js/jquery.dataTables.min.js"></script>
		<script src="js/dataTables.bootstrap.min.js"></script>

		<script src="js/jquery.inputmask.js"></script>
		<script src="js/jquery.inputmask.date.extensions.js"></script>
		<script src="js/jquery.inputmask.extensions.js"></script>

		<script>
	$("#dataPesquisa").inputmask("mm/yyyy", {
		"placeholder" : "mm/yyyy"
	});

	$("#dataPesquisaModal").inputmask("mm/yyyy", {
		"placeholder" : "mm/yyyy"
	});
	

	$("#dataPesquisaModal2").inputmask("mm/yyyy", {
		"placeholder" : "mm/yyyy"
	});
	
		$(function() {
			$('#tabela').DataTable({
				"paging" : true,
				"lengthChange" : false,
				"searching" : true,
				"ordering" : false,
				"info" : true,
				"autoWidth" : false
			});
		});
		
		
		function pagar(cod){
			if(window.confirm("Realizar pagamento ?")){
				document.getElementById("codDespesa").value = cod;
				document.getElementById("formsPaga").submit();
			}	
		}
		
		function excluir(cod){
			if(window.confirm("Excluir pagamento ?")){
				document.getElementById("codDespesaCanc").value = cod;
				document.getElementById("formsCanc").submit();
			}	
		}
		
		function pesquisa(){
		    document.getElementById("forms").submit();
		}
	</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="Java" import="br.com.gerenciador.dao.EntradaDAO"%>

<%@ page language="Java"
	import="br.com.gerenciador.conexao.ConnectionFactory"%>
<%@ page language="Java" import="br.com.gerenciador.model.Entrada"%>
<%@ page language="Java" import="br.com.gerenciador.model.Usuario"%>
<%@ page language="Java" import="java.sql.Connection"%>
<%@ page language="Java" import="java.util.ArrayList"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script type="text/javascript">
	
<%Connection conexao = (Connection) new ConnectionFactory().getConnection();
			Usuario usuario = (Usuario) session.getAttribute("usuariologado");
			if (conexao != null) {
				EntradaDAO dao = new EntradaDAO(conexao);
				ArrayList<Entrada> entradas = dao.select(usuario.getCod());

				session.setAttribute("entradas", entradas);
			} else {
				System.out.println("Conexao Null tabela_entrada.jsp atualizando ");
			}%>
	
</script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Creative - Bootstrap 3 Responsive Admin Template">
<meta name="author" content="GeeksLabs">
<meta name="keyword"
	content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
<link rel="shortcut icon" href="img/favicon.png">

<!-- DataTables -->
<link rel="stylesheet"
	href="http://45.55.154.243/MinhasFinancas/css/dataTables.bootstrap.css">

<title>Tabela de despesas</title>

<!-- Bootstrap CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- bootstrap theme -->
<link href="css/bootstrap-theme.css" rel="stylesheet">

<!-- DataTables -->
<link rel="stylesheet" href="css/dataTables.bootstrap.css">
<!--external css-->
<!-- font icon -->
<link href="css/elegant-icons-style.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<!-- Custom styles -->
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
</head>

<body>
	<!-- container section start -->
	<section id="container" class="">
		<!--header start-->

		<!--header end-->

		<!--sidebar start-->
		<c:import url="menu.jsp"></c:import>

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">
							<i class="fa fa-table"></i> Entrada
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="index.jsp">P�gina
									Principal</a></li>
							<li><i class="fa fa-table"></i>Tabelas</li>
							<li><i class="fa fa-th-list"></i>De Entrada</li>
						</ol>
					</div>
				</div>
				<!-- page start-->
				<div class="row">
					<div class="col-sm-2">
					<form action="Financas" method="post" id="forms">
					<input name="acao" value="AjaxEntrada" type="hidden">
					<input type="hidden" name="tipo" value="entrada">
						<input type="text" class="form-control pull-right" name="acao1"
							id="dataPesquisa" pattern="\d{2}/\d{4}" data-inputmask="'alias': 'mm/yyyy'" data-mask
							placeholder="Data para pesquisa">
					</form>		
					</div>
					<div class="col-sm-2">
					<button onclick="pesquisa()" class="btn btn-primary"><span class="icon_lightbulb_alt"></span> Pesquisa</button>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
						 <div class="table-responsive">
						
							<header class="panel-heading"> Tabela de Entrada </header>
							<table id="tabela" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Descri��o</th>
										<th>Valor</th>
										<th>Situa��o</th>
										<th>Data Incus�o</th>
										<th>Data Cancelado</th>
										<th>Excluir Receita</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="entrada" items="${entradas}" varStatus="id">
										<tr>
											<td>${entrada.descricao}</td>
											<td>${entrada.valor}
											<td>${entrada.situacao}</td>
											<td>${entrada.dataInclusao}</td>
											<td>${entrada.dataCancelado}</td>
											<c:choose>
												<c:when test="${entrada.situacao  != 'Ativo'}">
													<td><button disabled="disabled" onclick=''
															type="button" class="btn btn-danger">
															<span class="fa fa-times"></span>
														</button></td>
												</c:when>

												<c:when test="${entrada.situacao  == 'Ativo'}">
													<td><button   onclick='excluir(${entrada.cod})'
															type="button" class="btn btn-danger">
															<span class="fa fa-times"></span>
														</button></td>
												</c:when>
											</c:choose>
										</tr>
									</c:forEach>
								</tbody>
							</table>
</div>

						</section>
					</div>
				</div>
				<!-- page end-->
			</section>
		</section>
		<!--main content end-->
	</section>
	<form action="Financas" method="post" id="formsCanc">
			<input name="acao" value="ExcluirEntrada" type="hidden"> <input
				type="hidden" name="tipo" value="entrada"> <input
				type="hidden" name="acao1" id="codEntradaCanc">
		</form>
	<!-- container section end -->
	<!-- javascripts -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- nicescroll -->
	<script src="js/jquery.scrollTo.min.js"></script>
	<script src="js/jquery.nicescroll.js" type="text/javascript"></script>
	<!--custome script for all page-->
	<script src="js/scripts.js"></script>


	<!-- DataTables -->
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/dataTables.bootstrap.min.js"></script>

	<script src="js/jquery.inputmask.js"></script>
	<script src="js/jquery.inputmask.date.extensions.js"></script>
	<script src="js/jquery.inputmask.extensions.js"></script>
	<script>
		$("#dataPesquisa").inputmask("mm/yyyy", {
			"placeholder" : "mm/yyyy"
		});

		$(function() {
			$('#tabela').DataTable({
				"paging" : true,
				"lengthChange" : false,
				"searching" : true,
				"ordering" : false,
				"info" : true,
				"autoWidth" : false
			});
		});
		
		function pesquisa(){
		    document.getElementById("forms").submit();
		}
		
		function excluir(cod){
			if(window.confirm("Excluir receita ?")){
				document.getElementById("codEntradaCanc").value = cod;
				document.getElementById("formsCanc").submit();
			}	
		}
	</script>

</body>
</html>

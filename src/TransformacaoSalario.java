

import java.math.BigDecimal;

public class TransformacaoSalario {

	public BigDecimal tranformacaoSalarioParaDouble(String salarioString){
		double salario=0;
		String salariocopia = new String();
		int i=0;
		System.out.println(salarioString);
		if(salarioString.contains(",")){
		while(salarioString.length()>i){
			if(!salarioString.substring(i, i+1).equalsIgnoreCase(",")){
				salariocopia+= salarioString.substring(i, i+1);	
			}
			i++;
		}
		}

		return salariocopia.length() > 0 ?  new BigDecimal(salariocopia) : new BigDecimal(salarioString);
	}
	
	public double VerificarDinherio(double salario){
		String salariocopia = new String();
		String salarioString = String.valueOf(salario);
		int i=0,cont=0;
		while(salarioString.length()>i){
			if(salarioString.substring(i, i+1).equalsIgnoreCase(".")){
			 cont++;
			}
			i++;
		}
		if(cont==0){
			salariocopia += ".00";
		}
		return Double.valueOf(salariocopia);
	}
	
}

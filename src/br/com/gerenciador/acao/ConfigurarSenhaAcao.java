package br.com.gerenciador.acao;

import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.dao.UsuarioDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Usuario;

public class ConfigurarSenhaAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Connection conexao = (Connection) request.getAttribute("conexao");
		UsuarioDAO dao = new UsuarioDAO(conexao);
		HttpSession session = request.getSession();
		
		Usuario usuario = (Usuario)session.getAttribute("usuariologado");	
		String senhaAntiga = request.getParameter("antiga");
		String nova = request.getParameter("nova");
		String conf = request.getParameter("conf");
		String senha_sgbd_criptografada = dao.retornaSenhaSGBD(usuario.getEmail());
		boolean sucesso = false;
		if(senha_sgbd_criptografada!=null){
			if(dao.Descriptografar(senhaAntiga, senha_sgbd_criptografada)){
				if(nova.equalsIgnoreCase(conf)){
					dao.AlterarSenha(usuario.getCod(), dao.Criptografar(nova));
					sucesso = true;
				}
			}
		}
		
		
		if(!sucesso) request.setAttribute("msgm", "Senha antiga n�o estar correto!");
		
		
		RequestDispatcher rd = request.getRequestDispatcher("configurar_senha.jsp");
		rd.forward(request, response);
		
	}

}

package br.com.gerenciador.acao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 



/**
 *
 * @author Felipe
 */
public class CorreioAcao {
    String senhaEmail = new String();
    String LoginEmail = new String();
    public CorreioAcao(){
        
    }
    public boolean email(final String de,final String senhaDe,String remetente,String titulo, String mensagem){
        
    Properties props = new Properties();
            /** Par�metros de conex�o com servidor Hotmail */
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", "smtp.live.com");
            props.put("mail.smtp.socketFactory.port", "587");
            props.put("mail.smtp.socketFactory.fallback", "false");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "587");
    
            
            Session session = Session.getDefaultInstance(props,
                        new javax.mail.Authenticator() {
                             protected PasswordAuthentication getPasswordAuthentication() 
                             {
                                   return new PasswordAuthentication(de, senhaDe);
                             }
                        });

            /** Ativa Debug para sess�o */
            session.setDebug(true);

            try {

                  Message message = new MimeMessage(session);
                  message.setFrom(new InternetAddress(remetente)); //Remetente

                  message.setRecipients(Message.RecipientType.TO, 
                                    InternetAddress.parse(remetente)); //Destinat�rio(s)
                  message.setSubject(titulo);//titulo
                  message.setText(mensagem);//mensagem 
                  /**M�todo para enviar a mensagem criada*/
                  Transport.send(message);

                  return true;
             } catch (MessagingException e) {
                 return false;
            }
      }

}

package br.com.gerenciador.acao;

import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.gerenciador.dao.UsuarioDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Usuario;

public class CriarContaAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Connection conexao = (Connection) request.getAttribute("conexao");
		UsuarioDAO dao = new UsuarioDAO(conexao);
		String email = request.getParameter("email");
		String senha = request.getParameter("senha");
		email = email.toLowerCase();
		String cpf = request.getParameter("cpf");
		String dataNas = request.getParameter("datanasc");
		String nome = request.getParameter("nome");
		String uf = request.getParameter("uf");
		String msgm = null;
		Object test = dao.selectCodEDataInclusaoPorEmail(email);
		if(test==null){
			
		Usuario usuario = new Usuario();
		usuario.setCpf(cpf);
		usuario.setDataNasc(dataNas);
		usuario.setNome(nome);
		usuario.setEmail(email);
		usuario.setUf(uf);
		 //dataAtual = dao.selectDataAtual();
		usuario.setSenha(dao.Criptografar(senha.trim()));
		dao.insert(usuario);
		
		
		request.setAttribute("msgUsuario", "Cadastrado com sucesso!!!");
		}else if(test!=null){
			//msgm = "Email ja cadastrado em nosso banco de dados.";
			request.setAttribute("msgUsuario", "Email j� existe em nosso banco de dados");
		}
	/*
		Runnable correio = new CorreioEletronico(new Long(senha.trim()),email,msgm);
		Thread thread = new Thread(correio);
		thread.start();
		
		Runnable correio2 = new CorreioEletronico(new Long(senha.trim()),"felipe_dieb@hotmail.com","Novo Usuario : " + nome + " Uf : " + uf + " Email : "+ email);
		Thread thread2 = new Thread(correio2);
		thread2.start();
		*/
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.forward(request, response);
		
	}
	
	public String criarSenha(String data){
		
		String senha = new String();
		data = data.replaceAll("-", "");
	
		senha += data.substring(2, 4);
		senha+="5";
		senha += data.substring(4,6);
		senha += "14";
		senha+=data;
		
		return senha;
	}
	public class CorreioEletronico implements Runnable {
		 private Long senha ;
		 private CorreioAcao correio = null;
		 private String email;
		 private String mensagem;
		 public CorreioEletronico(Long senhaAleatorio,String email,String msgm){
			 senha = senhaAleatorio;
			 correio = new CorreioAcao();
			 this.email = email;
			 this.mensagem = msgm;
		 }
		   public void run () {
				correio.email("felipe_dieb@hotmail.com", "amuhfrd9110", email, "Conta criada com sucesso!", mensagem);
		   }
		 }


}

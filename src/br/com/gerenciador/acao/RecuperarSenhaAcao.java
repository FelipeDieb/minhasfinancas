package br.com.gerenciador.acao;

import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.dao.UsuarioDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Usuario;

public class RecuperarSenhaAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Connection conexao = (Connection) request.getAttribute("conexao");
		UsuarioDAO dao = new UsuarioDAO(conexao);
		String email = request.getParameter("email");
		String cpf = request.getParameter("cpf");
		String senha = request.getParameter("senha");
		String datanasc = request.getParameter("datanasc");
		if(dao.selectCodEDataInclusaoPorEmail(email,cpf,datanasc)){
			String cod = dao.selectCodEDataInclusaoPorEmail(email);
			dao.AlterarSenha(new Long(cod), dao.Criptografar(senha));
			request.setAttribute("msgUsuario", "Senha alterada com sucesso!!!");
		}else{
			request.setAttribute("msgUsuario", "Usuario n�o existe");
		}
		//String senhaCriada = criarSenha(lista);
		/*
		Runnable correio = new CorreioEletronico(new Long(senhaCriada),email);
		Thread thread = new Thread(correio);
		thread.start();
		*/
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.forward(request, response);
		
	}
	
	public String criarSenha(ArrayList<String> lista){
		
		String cod = lista.get(0);
		String data = lista.get(1);
		String senha = new String();
		data = data.replaceAll("-", "");
	
		senha += data.substring(2, 4);
		senha += data.substring(4,6);
		senha += cod;
		senha+=data;
		
		return senha;
	}
	public class CorreioEletronico implements Runnable {
		 private Long senha ;
		 private CorreioAcao correio = null;
		 private String email;
		 public CorreioEletronico(Long senhaAleatorio,String email){
			 senha = senhaAleatorio;
			 correio = new CorreioAcao();
			 this.email = email;
		 }
		   public void run () {
			   String mensagem = "Sua nova senha � : " + senha.toString();
				correio.email("felipe_dieb@hotmail.com", "amuhfrd9110", email, "Recupera��o de senha MinhasFinancas", mensagem);
		   }
		 }


}

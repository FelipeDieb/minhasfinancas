package br.com.gerenciador.acao;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.model.Acao;

public class SairAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		RequestDispatcher rd = request.getRequestDispatcher("/login.jsp");
		HttpSession session = request.getSession();
		session.invalidate();
		rd.forward(request, response);
		
	}

}

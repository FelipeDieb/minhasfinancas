package br.com.gerenciador.acao;

import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.dao.BancoDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Usuario;

public class ValorPlanejadoAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Connection conexao = (Connection) request.getAttribute("conexao");
		BancoDAO dao = new BancoDAO(conexao);
		String valor = request.getParameter("valor");
		HttpSession session = request.getSession();
		Usuario usuario = (Usuario)session.getAttribute("usuariologado");
		dao.updateValorPlano(usuario.getCod(), new TransformacaoSalario().tranformacaoSalarioParaDouble(valor));
		
		RequestDispatcher rd = request.getRequestDispatcher("planejamento.jsp");
		rd.forward(request, response);
		
	}

}

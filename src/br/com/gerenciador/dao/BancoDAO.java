package br.com.gerenciador.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BancoDAO {

	Connection conexao = null;

	public BancoDAO(Connection conexao) {
		this.conexao = conexao;
	}

	public String selectValor(Long codUsuario) {

		try {
			PreparedStatement pstm = conexao.prepareStatement("select valor from banco where usuario = ? "
					+ " and to_char(current_date,'mm/yyyy') = to_char(cast(data_inclusao as date),'mm/yyyy')");
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				return rs.getBigDecimal("valor").toString();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void updateValorPlano(Long codUsuario, BigDecimal valor) {

		try {
			PreparedStatement pstm = conexao.prepareStatement("update   plano set valor = ? where usuario = ? "
					+ " and to_char(current_date,'mm/yyyy') = to_char(cast(data_inclusao as date),'mm/yyyy')");
			pstm.setBigDecimal(1, valor);
			pstm.setLong(2, codUsuario);
			System.out.println(pstm.toString());
			pstm.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public String ultimoDiaDoMes() {
		try {
			PreparedStatement pstm = conexao.prepareStatement(
					"SELECT to_char(date_trunc('month',current_date) + INTERVAL'1 month' - INTERVAL'1 day','dd') as dia");
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				return rs.getString("dia");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String diaAtual() {
		try {
			PreparedStatement pstm = conexao.prepareStatement(
					"select to_char(current_date,'dd') as dia");
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				return rs.getString("dia");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String selectValorDespesaMensal(Long codUsuario) {

		try {
			PreparedStatement pstm = conexao
					.prepareStatement("select sum(valor) as total from despesas where usuario = ? "
							+ " and to_char(current_date,'mm/yyyy') = to_char(cast(data_inclusao as date),'mm/yyyy')"
							+ "  having sum(valor) is not null");
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				return rs.getBigDecimal("total").toString();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "0";
	}
	
	public String selectValorPlanejadoMensal(Long codUsuario) {

		try {
			PreparedStatement pstm = conexao
					.prepareStatement("select valor  from plano where usuario = ? "
							+ " and to_char(current_date,'mm/yyyy') = to_char(cast(data_inclusao as date),'mm/yyyy')");
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				return rs.getBigDecimal("valor").toString();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String selectValorEntradaMensal(Long codUsuario) {

		try {
			PreparedStatement pstm = conexao
					.prepareStatement("select sum(valor) as total from entrada where usuario = ? "
							+ " and to_char(current_date,'mm/yyyy') = to_char(cast(data_inclusao as date),'mm/yyyy')"
							+ " having sum(valor) is not null ");
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				return rs.getBigDecimal("total").toString();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "0";
	}
}

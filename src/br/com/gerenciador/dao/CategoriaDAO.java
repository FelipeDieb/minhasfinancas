package br.com.gerenciador.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.gerenciador.model.Categorias;

public class CategoriaDAO {

	Connection conexao = null;

	public CategoriaDAO(Connection conexao) {
		this.conexao = conexao;
	}

	public Long selectCod(String categoria){
		
		try {
			PreparedStatement pstm = conexao.prepareStatement("select cod from categorias "
					                                        + " where categoria = ?");
			pstm.setString(1, categoria);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if(rs.next()){
				return rs.getLong("cod");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public void insert(Categorias categoria) {

		try {
			PreparedStatement pstm = conexao
					.prepareStatement("insert into categorias(categoria,data_inclusao) " + " values (?,current_date)");
			pstm.setString(1, categoria.getCategoria());
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Long selectTemp(){
		try {
			PreparedStatement pstm = conexao.prepareStatement("select cod from categorias where categoria = 'Temporário'");
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if(rs.next()){
				return rs.getLong("cod");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public ArrayList<Categorias> select() {

		ArrayList<Categorias> lista = new ArrayList<Categorias>();
		Categorias categoria = null;
		try {
			PreparedStatement pstm = conexao
					.prepareStatement("select * from categorias");
			
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			
			while(rs.next()){
				categoria = new Categorias();
				categoria.setCategoria(rs.getString("categoria"));
				categoria.setCod(rs.getLong("cod"));
				lista.add(categoria);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}

}

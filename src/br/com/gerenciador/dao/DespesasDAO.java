package br.com.gerenciador.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.gerenciador.model.Categorias;
import br.com.gerenciador.model.Despesas;

public class DespesasDAO {

	Connection conexao = null;

	public DespesasDAO(Connection conexao) {
		this.conexao = conexao;
	}


	public String nomeCat(Long codCategoria){
		try {
			PreparedStatement pstm = conexao.prepareStatement("select categoria from categorias where cod = ?");
			pstm.setLong(1, codCategoria);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if(rs.next()){
				return rs.getString("categoria");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public void insert(Despesas despesa) {

		try {
			PreparedStatement pstm = conexao
					.prepareStatement("insert into despesas(nome,valor,data_inclusao,situacao,categoria,usuario,data_inicio,data_termino) "
							+ " values (?,?,current_date,'Aguardando Pagamento',?,?,cast(? as date),cast(? as date))");
			pstm.setString(1, despesa.getNome());
			pstm.setBigDecimal(2, despesa.getValor());
			pstm.setLong(3, despesa.getCategoria().getCod());
			pstm.setLong(4, despesa.getUsuario());
			pstm.setString(5, despesa.getDataInicio());
			pstm.setString(6, despesa.getDataTermino());
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	

	
	public void insertTemporario(Despesas despesa) {

		String sql = "select inserttempParcelado(?,?, ?, ?,?)";
		try {
			PreparedStatement pstm = conexao
					.prepareStatement(sql);
			pstm.setString(1, despesa.getNome());
			pstm.setBigDecimal(2, despesa.getValor());
			pstm.setLong(3, despesa.getCategoria().getCod());
			pstm.setLong(4, despesa.getUsuario());
			pstm.setInt(5, Integer.parseInt(despesa.getParcelas()));
			//pstm.setString(5, despesa.getDataInicio());
			//pstm.setString(6, despesa.getDataTermino());
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public BigDecimal valorEmBanco(Long codUsuario){
		try {
			PreparedStatement pstm = conexao.prepareStatement("select valor from banco where usuario = ?");
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if(rs.next()){
				return rs.getBigDecimal("valor");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public BigDecimal valorConta(Long codUsuario,Long codDespesas){
		try {
			PreparedStatement pstm = conexao.prepareStatement("select valor from despesas where usuario = ?"
					                                        + " and cod = ?");
			pstm.setLong(1, codUsuario);
			pstm.setLong(2, codDespesas);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if(rs.next()){
				return rs.getBigDecimal("valor");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<Despesas> selectAjax(Long codUsuario,String data) {

		ArrayList<Despesas> lista = new ArrayList<Despesas>();
		Despesas despesa = null;
		Categorias categoria = null;
		try {
			PreparedStatement pstm = conexao
					.prepareStatement("select d.*, c.categoria as nomecat,c.cod as codcat "
							        + " from despesas d, categorias c  "
							        + " where d.usuario = ?  and d.categoria = c.cod  "
							        + " and (to_char(d.data_inclusao,'mm/yyyy') = to_char(cast(? as date),'mm/yyyy')/*or c.categoria = 'Constante' */   "
							        + " or ( to_date(to_char(d.data_inclusao,'mm/yyyy'),'mm/yyyy') < to_date(to_char(current_date,'mm/yyyy'),'mm/yyyy')) and d.situacao = 'Aguardando Pagamento')");
			pstm.setLong(1, codUsuario);
			pstm.setString(2, "01/"+data);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			
			while(rs.next()){
				despesa = new Despesas();
				categoria = new Categorias();

				categoria.setCategoria(rs.getString("nomecat"));
				categoria.setCod(rs.getLong("codcat"));
				despesa.setNome(rs.getString("nome"));
				despesa.setUsuario(rs.getLong("usuario"));
				despesa.setCod(rs.getLong("cod"));
				despesa.setValor(rs.getBigDecimal("valor"));
				despesa.setDataInclusao(rs.getString("data_inclusao"));
				despesa.setDataInicio(rs.getString("data_inicio"));
				despesa.setDataTermino(rs.getString("data_termino"));
				despesa.setSituacao(rs.getString("situacao"));
				despesa.setCategoria(categoria);
				lista.add(despesa);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}
	
	public ArrayList<Despesas> select(Long codusuario){
		ArrayList<Despesas> lista = new ArrayList<Despesas>();
		Despesas despesa = null;
		Categorias categoria = null;
		try {
			PreparedStatement pstm = conexao.prepareStatement("select d.*, c.categoria as nomecat,c.cod as codcat from despesas d, categorias c "
					                                        + " where d.usuario = ? "
					                                        + " and d.categoria = c.cod "
					                                        + " and (to_char(d.data_inclusao,'mm/yyyy') = to_char(current_date,'mm/yyyy')/*or c.categoria = 'Constante' */ "
					                                        + " or ( to_date(to_char(d.data_inclusao,'mm/yyyy'),'mm/yyyy') < to_date(to_char(current_date,'mm/yyyy'),'mm/yyyy')) and d.situacao = 'Aguardando Pagamento')");
			pstm.setLong(1, codusuario);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			while(rs.next()){
				despesa = new Despesas();
				categoria = new Categorias();
				
				despesa.setCod(rs.getLong("cod"));
				despesa.setNome(rs.getString("nome"));
				despesa.setValor(rs.getBigDecimal("valor"));
				despesa.setSituacao(rs.getString("situacao"));
				despesa.setDataInclusao(rs.getString("data_inclusao"));
				despesa.setDataInicio(rs.getString("data_inicio"));
				despesa.setDataTermino(rs.getString("data_termino"));
				categoria.setCategoria(rs.getString("nomecat"));
				categoria.setCod(rs.getLong("codcat"));
				despesa.setCategoria(categoria);
				lista.add(despesa);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}
	
	public void realizaPagamneto(Long cod){
		
		try {
			PreparedStatement pstm = conexao.prepareStatement("update despesas set situacao = 'Pago' where cod = ?");
			pstm.setLong(1, cod);
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
public void cancelarPagamneto(Long cod){
		
		try {
			PreparedStatement pstm = conexao.prepareStatement("update despesas set situacao = 'Cancelado' where cod = ?");
			pstm.setLong(1, cod);
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

public void excluirPagamneto(Long cod){
	
	try {
		PreparedStatement pstm = conexao.prepareStatement("delete from  despesas  where cod = ?");
		pstm.setLong(1, cod);
		System.out.println(pstm.toString());
		pstm.execute();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}

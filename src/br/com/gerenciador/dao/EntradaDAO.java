package br.com.gerenciador.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.gerenciador.model.Entrada;

public class EntradaDAO {

	Connection conexao = null;

	public EntradaDAO(Connection conexao) {
		this.conexao = conexao;
	}

	public void insert(Entrada entrada) {

		try {
			PreparedStatement pstm = conexao
					.prepareStatement("insert into entrada(descricao,valor,usuario,data_inclusao,situacao) " + " values (?,?,?,current_date,'Ativo')");
			pstm.setString(1, entrada.getDescricao());
			pstm.setBigDecimal(2,entrada.getValor());
			pstm.setLong(3, entrada.getUsuario());
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Entrada> select(Long codUsuario) {

		ArrayList<Entrada> lista = new ArrayList<Entrada>();
		Entrada entrada = null;
		try {
			PreparedStatement pstm = conexao
					.prepareStatement("select * from entrada where usuario = ? and to_char(data_inclusao,'mm/yyyy') = to_char(current_date,'mm/yyyy')");
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			
			while(rs.next()){
				entrada = new Entrada();
				entrada.setDescricao(rs.getString("descricao"));
				entrada.setUsuario(rs.getLong("usuario"));
				entrada.setCod(rs.getLong("cod"));
				entrada.setValor(rs.getBigDecimal("valor"));
				entrada.setDataInclusao(rs.getString("data_inclusao"));
				entrada.setSituacao(rs.getString("situacao"));
				entrada.setDataCancelado(rs.getString("data_cancelado"));
				lista.add(entrada);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}
	
	public ArrayList<Entrada> selectAjax(Long codUsuario,String data) {

		ArrayList<Entrada> lista = new ArrayList<Entrada>();
		Entrada entrada = null;
		try {
			PreparedStatement pstm = conexao
					.prepareStatement("select * from entrada where usuario = ? "
							        + " and to_char(data_inclusao,'mm/yyyy') = to_char(cast(? as date),'mm/yyyy')");
			pstm.setLong(1, codUsuario);
			pstm.setString(2, "01/"+data);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			
			while(rs.next()){
				entrada = new Entrada();
				entrada.setDescricao(rs.getString("descricao"));
				entrada.setUsuario(rs.getLong("usuario"));
				entrada.setCod(rs.getLong("cod"));
				entrada.setValor(rs.getBigDecimal("valor"));
				entrada.setDataInclusao(rs.getString("data_inclusao"));
				entrada.setDataCancelado(rs.getString("data_cancelado"));
				entrada.setSituacao(rs.getString("situacao"));
				lista.add(entrada);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}

	public void CancelaReceita(Long cod){
		try {
			PreparedStatement pstm = conexao.prepareStatement("update entrada set situacao = 'Desativado', data_cancelado = current_date where cod = ?");
			pstm.setLong(1, cod);
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void excluirReceita(Long cod){
		try {
			PreparedStatement pstm = conexao.prepareStatement("delete from  entrada where cod = ?");
			pstm.setLong(1, cod);
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

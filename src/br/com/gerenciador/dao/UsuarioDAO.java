package br.com.gerenciador.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.mindrot.jbcrypt.BCrypt;

import br.com.gerenciador.model.Usuario;

public class UsuarioDAO {

	Connection conexao = null;

	public UsuarioDAO(Connection conexao) {
		this.conexao = conexao;
	}

	public void AlterarSenha(Long codUsuario,String novaSenha){
		
		try {
			PreparedStatement pstm = conexao.prepareStatement("update usuario set senha = ? where cod = ?");
			pstm.setString(1, novaSenha);
			pstm.setLong(2, codUsuario);
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void verificaConstantes(Long codUsuario) {

		String sql = "select despesasconstantes(?)";
		try {
			PreparedStatement pstm = conexao
					.prepareStatement(sql);
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public void insertLogs(Long codUsuario) {

		String sql = "select insertlog(?)";
		try {
			PreparedStatement pstm = conexao
					.prepareStatement(sql);
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	public String usuarioExiste(Long codUsuario){
		
		try {
			PreparedStatement pstm = conexao.prepareStatement("select possuibanco(?) as resp");
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if(rs.next()){
				return rs.getString("resp");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public String usuarioPossuiPlano(Long codUsuario){
		
		try {
			PreparedStatement pstm = conexao.prepareStatement("select possuiplano(?) as resp");
			pstm.setLong(1, codUsuario);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if(rs.next()){
				return rs.getString("resp");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
     public String selectCodEDataInclusaoPorEmail(String email){
		String cod =null;
		try {
			PreparedStatement pstm = conexao.prepareStatement("select * from usuario where email = ?");
			pstm.setString(1, email);
			System.out.println(pstm.toString());
			ResultSet rs = pstm.executeQuery();
			if(rs.next()){
				cod = rs.getString("cod");
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cod;
	}
     
     

     public boolean selectCodEDataInclusaoPorEmail(String email,String cpf,String datanasc){
 		
 		try {
 			PreparedStatement pstm = conexao.prepareStatement("select * from usuario where email = ?"
 					                                        + " and cpf = ? and data_niver = ?");
 			pstm.setString(1, email);
 			pstm.setString(2, cpf);
 			pstm.setString(3, datanasc);
 			System.out.println(pstm.toString());
 			
 			ResultSet rs = pstm.executeQuery();
 			if(rs.next()){
 				return true;
 			}
 		} catch (SQLException e) {
 			e.printStackTrace();
 		}
 		return false;
 	}
	public void insert(Usuario usuario) {

		try {
			PreparedStatement pstm = conexao
					.prepareStatement("insert into usuario(nome,senha,email,data_inclusao,uf,data_niver,cpf) " 
			+ " values (?,?,?,current_date,?,?,?)");
			pstm.setString(1, usuario.getNome());
			pstm.setString(2, usuario.getSenha());
			pstm.setString(3, usuario.getEmail());
			pstm.setString(4, usuario.getUf());
			pstm.setString(5, usuario.getDataNasc());
			pstm.setString(6, usuario.getCpf());
			System.out.println(pstm.toString());
			pstm.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public String selectDataAtual() {
		PreparedStatement stmt;
		String senha = null;
		try {
			stmt = conexao.prepareStatement("select current_date  as data");
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				senha = rs.getString("data");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return senha;
	}
	
	public String retornaSenhaSGBD(String login) {
		PreparedStatement stmt;
		String senha = null;
		try {
			stmt = conexao.prepareStatement("select senha from usuario where email = ?");
			stmt.setString(1, login);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				senha = rs.getString("senha");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return senha;
	}
	
	public String Criptografar(String password_plaintext) {
		String salt = BCrypt.gensalt();
		String hashed_password = BCrypt.hashpw(password_plaintext, salt);

		return (hashed_password);
	}

	public static boolean Descriptografar(String password_plaintext, String stored_hash) {
		boolean password_verified = false;

		if (null == stored_hash || !stored_hash.startsWith("$2a$"))
			throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

		password_verified = BCrypt.checkpw(password_plaintext, stored_hash);

		return (password_verified);
	}
	
	public Usuario AutenticacaoAdm(String login, String senha) {

		PreparedStatement stmt;
		Usuario usuario = null;
		try {
			stmt = conexao.prepareStatement("select * from usuario where " + "email=? and senha=?");
			stmt.setString(1, login);
			stmt.setString(2, senha);
            System.out.println(stmt.toString());
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				usuario = new Usuario();
				usuario.setCod(new Long(rs.getInt("cod")));
				usuario.setNome(rs.getString("nome"));
				usuario.setEmail(rs.getString("email"));
				usuario.setSenha(rs.getString("senha"));
				usuario.setDataInclusao(rs.getString("data_inclusao"));
			}
			return usuario;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return usuario;

	}

	

}

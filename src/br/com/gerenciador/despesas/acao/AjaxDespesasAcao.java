package br.com.gerenciador.despesas.acao;

import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.dao.DespesasDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Despesas;
import br.com.gerenciador.model.Usuario;

public class AjaxDespesasAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {



		Connection conexao = (Connection) request.getAttribute("conexao");
		DespesasDAO dao = new DespesasDAO(conexao);
		HttpSession session = request.getSession();
		String data = request.getParameter("acao1");
		
		if(data.length() >= 7 && data.contains("/") && !(data.contains("y") || data.contains("m"))){
			
		Usuario usuario = (Usuario)session.getAttribute("usuariologado");	
		ArrayList<Despesas> lista = dao.selectAjax(usuario.getCod(), data);
		request.setAttribute("msgm", "Cadastrado com sucesso!!!");
		request.setAttribute("despesas", lista);
		}
	
		
		
		RequestDispatcher rd = request.getRequestDispatcher("tabela_despesa.jsp");
		rd.forward(request, response);
		
	}

}

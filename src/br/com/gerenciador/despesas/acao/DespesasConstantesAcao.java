package br.com.gerenciador.despesas.acao;

import java.math.BigDecimal;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.acao.TransformacaoSalario;
import br.com.gerenciador.dao.CategoriaDAO;
import br.com.gerenciador.dao.DespesasDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Categorias;
import br.com.gerenciador.model.Despesas;
import br.com.gerenciador.model.Usuario;

public class DespesasConstantesAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {



		HttpSession session = request.getSession();
		Usuario usuario = (Usuario)session.getAttribute("usuariologado");	
		Connection conexao = (Connection) request.getAttribute("conexao");
		DespesasDAO dao = new DespesasDAO(conexao);
		CategoriaDAO daoC = new CategoriaDAO(conexao);
		Despesas despesa = new Despesas();
		
		despesa.setNome(request.getParameter("nome"));
		String salario = request.getParameter("valor");		
		despesa.setValor(new TransformacaoSalario().tranformacaoSalarioParaDouble(salario));
		
		despesa.setUsuario(usuario.getCod());
		Categorias categoria = new Categorias();
		categoria.setCod(daoC.selectCod("Constante"));
		despesa.setCategoria(categoria);
		dao.insert(despesa);
		
		request.setAttribute("msgm", "Cadastrado com sucesso!!!");
		RequestDispatcher rd = request.getRequestDispatcher("despesas_constantes.jsp");
		rd.forward(request, response);
		
	}

}

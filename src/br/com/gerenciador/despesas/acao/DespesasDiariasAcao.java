package br.com.gerenciador.despesas.acao;

import java.math.BigDecimal;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.acao.TransformacaoSalario;
import br.com.gerenciador.dao.DespesasDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Categorias;
import br.com.gerenciador.model.Despesas;
import br.com.gerenciador.model.Usuario;

public class DespesasDiariasAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {



		HttpSession session = request.getSession();
		Usuario usuario = (Usuario)session.getAttribute("usuariologado");
		Connection conexao = (Connection) request.getAttribute("conexao");
		DespesasDAO dao = new DespesasDAO(conexao);
		
		Despesas despesa = new Despesas();
		String dataIni = null;
		String dataTer = null;
		despesa.setNome(request.getParameter("nome"));
		String salario = request.getParameter("valor");	
		String parcelas = request.getParameter("parcelas");
		despesa.setValor(new TransformacaoSalario().tranformacaoSalarioParaDouble(salario));
		Categorias categoria = new Categorias();
		categoria.setCod(new Long(request.getParameter("categoria")));
		despesa.setCategoria(categoria);
		/*if(dao.nomeCat(categoria.getCod()).equalsIgnoreCase("Temporário")){
			dataIni = request.getParameter("dataIni");
			dataTer = request.getParameter("dataTer");
		}
		*/
		
		despesa.setUsuario(usuario.getCod());
		despesa.setDataInicio(dataIni);
		despesa.setDataTermino(dataTer);
		despesa.setParcelas(parcelas);
		if(parcelas != null && Integer.parseInt(parcelas) > 1){
			dao.insertTemporario(despesa);
		}else{
			dao.insert(despesa);	
		}
		
		
		request.setAttribute("msgm", "Cadastrado com sucesso!!!");
		RequestDispatcher rd = request.getRequestDispatcher("despesas_diarias.jsp");
		rd.forward(request, response);
		
	}

}

package br.com.gerenciador.despesas.acao;

import java.math.BigDecimal;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.acao.TransformacaoSalario;
import br.com.gerenciador.dao.CategoriaDAO;
import br.com.gerenciador.dao.DespesasDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Despesas;
import br.com.gerenciador.model.Usuario;

public class ExcluirDespesaAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Connection conexao = (Connection) request.getAttribute("conexao");
		DespesasDAO dao = new DespesasDAO(conexao);
		String cod = request.getParameter("acao1");
		if(cod!=null)
		dao.excluirPagamneto(new Long(cod));
		request.setAttribute("msgm", "Exclu�do com sucesso!!!");
		RequestDispatcher rd = request.getRequestDispatcher("tabela_despesa.jsp");
		rd.forward(request, response);
		
	}

}

package br.com.gerenciador.despesas.acao;

import java.math.BigDecimal;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.acao.TransformacaoSalario;
import br.com.gerenciador.dao.CategoriaDAO;
import br.com.gerenciador.dao.DespesasDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Despesas;
import br.com.gerenciador.model.Usuario;

public class PagarDespesaAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		Usuario usuario = (Usuario)session.getAttribute("usuariologado");
		Connection conexao = (Connection) request.getAttribute("conexao");
		DespesasDAO dao = new DespesasDAO(conexao);
		String cod = request.getParameter("acao1");
		BigDecimal valorBanco = dao.valorEmBanco(usuario.getCod());
		BigDecimal valorConta = dao.valorConta(usuario.getCod(), new Long(cod));
		if(valorBanco.compareTo(valorConta) == 1 || valorBanco.compareTo(valorConta) == 0){
			dao.realizaPagamneto(new Long(cod));
			request.setAttribute("msgm", "Pago com sucesso!!!");
		}else{
			request.setAttribute("msgm", "Conta maior que seu Saldo.");
		}
		
		
		RequestDispatcher rd = request.getRequestDispatcher("tabela_despesa.jsp");
		rd.forward(request, response);
		
	}

}

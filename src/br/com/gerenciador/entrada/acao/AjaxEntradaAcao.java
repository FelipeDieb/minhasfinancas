package br.com.gerenciador.entrada.acao;

import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.acao.TransformacaoSalario;
import br.com.gerenciador.dao.EntradaDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Entrada;
import br.com.gerenciador.model.Usuario;

public class AjaxEntradaAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {



		Connection conexao = (Connection) request.getAttribute("conexao");
		EntradaDAO dao = new EntradaDAO(conexao);
		HttpSession session = request.getSession();
		String data = request.getParameter("acao1");
		
		if(data.length() >= 7 && data.contains("/") && !(data.contains("y") || data.contains("m"))){
			
		Usuario usuario = (Usuario)session.getAttribute("usuariologado");	
		ArrayList<Entrada> lista = dao.selectAjax(usuario.getCod(), data);
		request.setAttribute("msgm", "Cadastrado com sucesso!!!");
		request.setAttribute("entradas", lista);
		}
	
		
		
		RequestDispatcher rd = request.getRequestDispatcher("tabela_entrada.jsp");
		rd.forward(request, response);
		
	}

}

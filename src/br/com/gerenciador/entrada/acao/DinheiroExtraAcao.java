package br.com.gerenciador.entrada.acao;

import java.math.BigDecimal;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.acao.TransformacaoSalario;
import br.com.gerenciador.conexao.ConnectionFactory;
import br.com.gerenciador.dao.EntradaDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Entrada;
import br.com.gerenciador.model.Usuario;

public class DinheiroExtraAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {



		Connection conexao = (Connection) request.getAttribute("conexao");
		EntradaDAO dao = new EntradaDAO(conexao);
		HttpSession session = request.getSession();
		Usuario usuario = (Usuario)session.getAttribute("usuariologado");	
		Entrada entrada = new Entrada();
		
		entrada.setDescricao(request.getParameter("nome"));
		String salario = request.getParameter("valor");		
		entrada.setValor(new TransformacaoSalario().tranformacaoSalarioParaDouble(salario));
		
		
		entrada.setUsuario(usuario.getCod());
		dao.insert(entrada);
		
		request.setAttribute("msgm", "Cadastrado com sucesso!!!");
		RequestDispatcher rd = request.getRequestDispatcher("dinheiro_extra.jsp");
		rd.forward(request, response);
		
	}

}

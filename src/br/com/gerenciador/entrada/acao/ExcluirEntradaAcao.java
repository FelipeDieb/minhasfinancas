package br.com.gerenciador.entrada.acao;

import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.dao.EntradaDAO;
import br.com.gerenciador.model.Acao;

public class ExcluirEntradaAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Connection conexao = (Connection) request.getAttribute("conexao");
		EntradaDAO dao = new EntradaDAO(conexao);
		HttpSession session = request.getSession();
		String cod = request.getParameter("acao1");
		if(cod != null)
		dao.excluirReceita(new Long(cod));
		RequestDispatcher rd = request.getRequestDispatcher("tabela_entrada.jsp");
		rd.forward(request, response);
		
	}

	
}

package br.com.gerenciador.filtro;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import br.com.gerenciador.model.Usuario;



@WebFilter(filterName="AutenticacaoFiltro", urlPatterns="/*")
public class AutenticacaoFiltro implements Filter{

	@Override
	public void destroy() {
		
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		String uri = req.getRequestURI();
		String logica = request.getParameter("acao");
		logica+="Acao";
		if(logica == null)
		logica = "";
		System.out.println("Logica = " + logica);

		System.out.println("URI = " + uri);
		if( uri.equals("/MinhasFinancas/login.jsp")  || uri.equals("/MinhasFinancas/criar_conta.html") || uri.equals("/MinhasFinancas/recuperar_senha.html") ||   logica.endsWith("LoginAcao")  || uri.endsWith("js") || uri.endsWith("icon") || uri.endsWith("woff")
				|| logica.endsWith("CriarContaAcao") || logica.endsWith("RecuperarSenhaAcao") || logica.endsWith("AutenticacaoAcao")		|| uri.endsWith("png") || uri.endsWith("jpg") || uri.endsWith("css") ){
			chain.doFilter(request, response);
		}else{
			Usuario usuario = (Usuario) req.getSession().getAttribute("usuariologado");
			if(usuario!=null){
				chain.doFilter(request, response);
			}else{
				RequestDispatcher rd = request.getRequestDispatcher("/login.jsp");
				request.setAttribute("msgUsuario", " Voc� n�o estar logado !!!");
				rd.forward(request, response);
			}
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
		
	}

	
}

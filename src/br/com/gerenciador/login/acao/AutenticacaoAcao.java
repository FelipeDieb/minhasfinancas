package br.com.gerenciador.login.acao;

import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.conexao.ConnectionFactory;
import br.com.gerenciador.dao.UsuarioDAO;
import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Usuario;

public class AutenticacaoAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Connection conexao = (Connection) request.getAttribute("conexao");
		UsuarioDAO dao = new UsuarioDAO(conexao);
		Usuario usuario = null;
		String email = request.getParameter("email");
		email = email.toLowerCase();
		String senha = request.getParameter("senha");
		
		String senha_sgbd_criptografada = dao.retornaSenhaSGBD(email);
		
		if(senha_sgbd_criptografada!=null){
			String usuarioExiste = null;
			if(dao.Descriptografar(senha, senha_sgbd_criptografada)){
				usuario = dao.AutenticacaoAdm(email, senha_sgbd_criptografada);
				usuarioExiste = dao.usuarioExiste(usuario.getCod());
			}
			 
			
			if(usuario != null && usuarioExiste == null){
				dao.verificaConstantes(usuario.getCod());//pegar despesas constantes do mes anterior
				dao.usuarioPossuiPlano(usuario.getCod());
				dao.insertLogs(usuario.getCod());
				RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
				HttpSession session = request.getSession();
				session.setAttribute("usuariologado", usuario);	
				session.setAttribute("caminho", "http://45.55.154.243/MinhasFinancas/");
				session.setAttribute("conexaoSession", conexao);
				rd.forward(request, response);
			   
			}else{
				if(usuarioExiste == null){  usuarioExiste = "Senha do usu�rio incorreto";}
					
					
				RequestDispatcher rd = request.getRequestDispatcher("/login.jsp");
				request.setAttribute("msgUsuario", usuarioExiste);
				rd.forward(request, response);
			}
		}else{
			RequestDispatcher rd = request.getRequestDispatcher("/login.jsp");
			request.setAttribute("msgUsuario", "Email do usu�rio incorreto");
			rd.forward(request, response);
		}
		
	}

}

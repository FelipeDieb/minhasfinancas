package br.com.gerenciador.model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Acao {

	public void execute(HttpServletRequest request , HttpServletResponse response)throws Exception;
}

package br.com.gerenciador.model;

import java.math.BigDecimal;

public class Entrada {

	Long cod;
	String descricao;
	BigDecimal valor;
	String dataInclusao;
	Long usuario;
	String situacao;
	String dataCancelado;
	
	
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getDataCancelado() {
		return dataCancelado;
	}
	public void setDataCancelado(String dataCancelado) {
		this.dataCancelado = dataCancelado;
	}
	public Long getUsuario() {
		return usuario;
	}
	public void setUsuario(Long usuario) {
		this.usuario = usuario;
	}
	public Long getCod() {
		return cod;
	}
	public void setCod(Long cod) {
		this.cod = cod;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public String getDataInclusao() {
		return dataInclusao;
	}
	public void setDataInclusao(String dataInclusao) {
		this.dataInclusao = dataInclusao;
	}
	
	
}

package br.com.gerenciador.relatorio.acao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.gerenciador.model.Acao;
import br.com.gerenciador.model.Usuario;
import net.sf.jasperreports.engine.JasperPrint;

public class GerarRelatorioAcao implements Acao{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		    
		    Connection conexao = (Connection) request.getAttribute("conexao");
		    String nomeRelatorio = request.getParameter("nomeRelatorio");
		    HttpSession session = request.getSession();
		    String msgmSucesso = "Relatório gerado com sucesso!!!";
		    String msgmError = null;

		    String dataInicio = request.getParameter("dataInicio");
		    String dataFim = request.getParameter("dataFim");
			Usuario usuario = (Usuario)session.getAttribute("usuariologado");
		    Map<String, Object> parametros = new HashMap<String, Object>();
		    //ImageIcon gto = new ImageIcon(getClass().getResource("/MinhasFinancas/WebContent/dist/img/avatar.png"));
		    InputStream inputStreamDaImagem = null; 
		    System.out.println(request.getRealPath("/dist/img/logo.jpg"));
			String caminhoImagem = request.getRealPath("/dist/img/logo.jpg");//recebe o caminho da imagem
			try {  
				File file = new File(caminhoImagem); 
				if(file.exists())//testa se imagem existe
				inputStreamDaImagem = new FileInputStream(file);     
			} catch (FileNotFoundException e) {     
			   e.printStackTrace();     
			}  
		    parametros.put("imagem", inputStreamDaImagem);
		    parametros.put("codUsuario", String.valueOf(usuario.getCod()));
            parametros.put("dataInicio", dataInicio);
            parametros.put("dataFim", dataFim);
		    JasperPrint print;
		    String nome = request.getServletContext().getRealPath("/WEB-INF/jasper/"+nomeRelatorio+".jasper");
			try {
				
	            GeradorRelatorio gerador = new GeradorRelatorio(nome, parametros, conexao);
	            
	            response.setContentType("application/pdf");
	            
	            Random geradorAle = new Random();
	            
	            Calendar cal = Calendar.getInstance();
	            String date = null;
	            SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy_hh:mm:ss");
	    		date = String.valueOf(sdf.format(new Date()));
	            System.out.println(date);
				response.setHeader("Content-disposition", "filename=\""+nomeRelatorio+date+".pdf\"");
	            gerador.geraPDFParaOutputStream(response.getOutputStream());
			}catch (FileNotFoundException e2) {
				msgmError = "Error ao gerar Relatorio!!!";
				msgmSucesso = null;
				e2.printStackTrace();
			}

			
	}

}

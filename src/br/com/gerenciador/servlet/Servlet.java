package br.com.gerenciador.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.gerenciador.model.Acao;

@WebServlet("/Financas")
public class Servlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String acao = request.getParameter("acao");
		String tipo = request.getParameter("tipo");
		String nomaClasse = null;
		if(tipo==null||tipo.isEmpty()){
			 nomaClasse = "br.com.gerenciador.acao."+acao+"Acao"; 
		}else{
			 nomaClasse = "br.com.gerenciador."+tipo+".acao."+acao+"Acao";
			 System.out.println("Caminho :  " + nomaClasse);
		}
		
		
		try {
			Class classe = Class.forName(nomaClasse);
			Object obj = classe.newInstance();
			Acao inter = (Acao) obj;
			inter.execute(request, response);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
